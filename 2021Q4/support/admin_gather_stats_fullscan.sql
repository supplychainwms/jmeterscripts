/**
Notes from BY Database Best Practice

Index Rebuilds and Update Statistics
BY recommends index reorganizing and rebuilding, along with full database Update Statistics on a daily basis during off-peak hours. Other database maintenance tasks must be scheduled accordingly during off-peak hours.
Index Maintenance is necessary to reduce fragmentation on the disk. When indexes become fragmented, they are not as effective and additional overhead is encountered in Insert, Update and Delete operations against the tables where those indexes are in place. Heavily fragmented indexes can degrade query performance and cause your application to respond slowly.
Current Microsoft best practices recommend rebuilding indexes where average fragmentation is greater than 30%, and reorganizing indexes where it is below 30% but larger than 5% and the number of records in the table is greater than 1000 pages. Smaller tables (less than 1000 pages) do not benefit from reindexing operations.
Auto-Update statistics must be disabled.
Statistics are recommeded to be gathered daily with 100% sampling rate where possible. A limited set of hot tables may need statistics gathered more frequestly with FULL SCANS (100% sampling):
INVDTL, INVLOD, INVSUM, LMSTRN, PCKMOV, PCKWRK_HDR, PCKWRK_DTL, RIMLIN, SHIPMENT, SHIPMENT_LINE, INVMOV, INSUM, ORD, ORD_LINE, PRTMST, RCVLIN, PPRFTP_DTL, DLYTRN, RCVINV, QVLWRK, RCVTRK, RIMHDR


[select *
   from sys.tables
  where name in ('INVDTL', 'INVLOD', 'INVSUM', 'LMSTRN', 'PCKMOV', 'PCKWRK_HDR', 'PCKWRK_DTL', 'RIMLIN', 'SHIPMENT', 'SHIPMENT_LINE', 'INVMOV', 'INSUM', 'ORD', 'ORD_LINE', 'PRTMST', 'RCVLIN', 'PPRFTP_DTL', 'DLYTRN', 'RCVINV', 'QVLWRK', 'RCVTRK', 'RIMHDR')]
**/

exec sp_msforeachtable 'UPDATE STATISTICS ? WITH FULLSCAN';
